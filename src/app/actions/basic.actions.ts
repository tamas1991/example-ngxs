import { Basic } from '../basic.model';

export class AddBasic {
    static readonly type = '[Basic] AddBasic';

    constructor(public payload: Basic) { }
}

export class RemoveBasic {
    static readonly type = '[Basic] RemoveBasic';

    constructor(public id: number) { }
}