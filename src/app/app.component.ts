import { Component } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { BasicState } from './basic.state';
import { Basic } from './basic.model';
import { AddBasic, RemoveBasic } from './actions/basic.actions';
import { NegateAll } from './actions/very-basic-actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ngxs';

  @Select(BasicState.ids)
  public ids: Observable<number[]>;
  @Select(BasicState.names)
  public names: Observable<string[]>;

  constructor(private store: Store) {

  }

  addBasic() {
    let basic = new Basic();
    basic.id = Math.floor(Math.random() * 100);
    basic.name = `Ez itt tök egyedi - ${basic.id}`;
    this.store.dispatch(new AddBasic(basic)).subscribe(data => {
      console.log('-- data --\n', data);
    })
    
  }
  removeBasic() {
    let id = this.store.selectSnapshot(BasicState.ids)[0];
    this.store.dispatch(new RemoveBasic(id));
  }

  negateAll() {
    this.store.dispatch(new NegateAll()).subscribe(data => {
      console.log('-- very basic data --\n', data);
    });
  }

}
