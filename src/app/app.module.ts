import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';

import { AppComponent } from './app.component';
import { BasicState } from './basic.state';
import { VeryBasicState } from './state/very-basic.state';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { StarterComponent } from './test_1/starter/starter.component';
import { WorkerComponent } from './test_1/worker/worker.component';
import { WorkerFormState } from './test_1/worker/worker-form.state';

@NgModule({
  declarations: [
    AppComponent,
    StarterComponent,
    WorkerComponent
  ],
  imports: [
    BrowserModule,
    NgxsModule.forRoot([BasicState, VeryBasicState, WorkerFormState]),
    NgxsLoggerPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot()
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
