export class ValidateWorkerForm {
    public static readonly type = '[WORKER_FORM] Validate';
}

export class SetValidationState {
    public static readonly type = '[WORKER_FORM] Set validation state';
    constructor(public isValid: boolean) {}
}
