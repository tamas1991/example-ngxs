export class WorkerFormModel {

    public static withValues(id: number, name: string, age: number): WorkerFormModel {
        const data = new WorkerFormModel();
        data.id = id;
        data.name = name;
        data.age = age;
        data.valid = false;
        return data;
    }

    id: number;
    name: string;
    age: number;

    valid: boolean;

}