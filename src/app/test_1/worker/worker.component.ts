import { Component, OnInit } from '@angular/core';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import { Validate } from '../test_1.actions';
import { SetValidationState } from './worker-form.actions';

@Component({
  selector: 'app-worker',
  templateUrl: './worker.component.html',
  styleUrls: ['./worker.component.scss']
})
export class WorkerComponent implements OnInit {

  constructor(private actions$: Actions, private store: Store) {
    this.actions$
      .pipe(ofActionDispatched(Validate))
      .subscribe(() => {
        this.store.dispatch(new SetValidationState(true));
      });
  }

  ngOnInit() {
  }

}
