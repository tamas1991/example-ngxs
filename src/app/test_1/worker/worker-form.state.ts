import { State, Action, StateContext } from "@ngxs/store";
import { WorkerFormModel } from './worker-form.model';
import { ValidateWorkerForm, SetValidationState } from './worker-form.actions';

@State<WorkerFormModel>({
    name: 'workerForm',
    defaults: WorkerFormModel.withValues(12, 'Pont Ő', 33)
})
export class WorkerFormState {


    @Action(ValidateWorkerForm)
    public validate(context: StateContext<WorkerFormModel>, action: ValidateWorkerForm) {
    }

    @Action(SetValidationState)
    public setValidationState(context: StateContext<WorkerFormModel>, action: SetValidationState) {
        const state = context.getState();
        const newState = WorkerFormModel.withValues(state.id, state.name, state.age);
        newState.valid = action.isValid;
        context.patchState(newState);
    }



}