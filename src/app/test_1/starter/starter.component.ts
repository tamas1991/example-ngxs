import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Validate } from '../test_1.actions';

@Component({
  selector: 'app-starter',
  templateUrl: './starter.component.html',
  styleUrls: ['./starter.component.scss']
})
export class StarterComponent implements OnInit {

  constructor(private store: Store) {
    setTimeout(() => {
      const isValidBefore = this.store.snapshot().workerForm.valid;
      console.log('isValidBefore: ' + isValidBefore);

      this.store.dispatch(new Validate())
        .subscribe(data => {
          const isValidAfter = data.workerForm.valid;
          console.log('isValidAfter: ' + isValidAfter);
        })
    }, 2000);
  }

  ngOnInit() {
  }

}
