import { State, Action, StateContext } from '@ngxs/store';
import { Validate } from './test_1.actions';

interface ValidationModel {
    valid: boolean;
}

@State<ValidationModel>({
    name: 'validation',
    defaults: {valid: false}
})
export class ValidationState {

    @Action(Validate)
    public validate(context: StateContext<ValidationModel>, action: Validate) {
    }

}