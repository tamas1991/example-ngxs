import { VeryBasic } from './models/very-basic.model';

export class Basic {
    static withValues(id: number, name: string): Basic {
        const basic = new Basic();
        basic.id = id;
        basic.name = name;
        return basic;
    }
    id: number;
    name: string;

}