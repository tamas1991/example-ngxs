import { VeryBasic } from '../models/very-basic.model';
import { State, Action, StateContext } from '@ngxs/store';
import { NegateAll } from '../actions/very-basic-actions';

export interface VeryBasicModel {
    veryBasic: VeryBasic[];
}

@State<VeryBasicModel>({
    name: 'veryBasic',
    defaults: {veryBasic: [new VeryBasic(true), new VeryBasic(false)]}
})
export class VeryBasicState {

    @Action(NegateAll)
    negateAll(context: StateContext<VeryBasicModel>, action: NegateAll) {
        const state = context.getState();
        const negatedVeryBasicArray = state.veryBasic.map((veryBasic: VeryBasic) => new VeryBasic(!veryBasic.isBasic));
        context.patchState({
            veryBasic: negatedVeryBasicArray
        });
    }


}