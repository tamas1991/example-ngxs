import { State, Selector, Store, Action, StateContext } from '@ngxs/store';
import { Basic } from './basic.model';
import { AddBasic, RemoveBasic } from './actions/basic.actions';
import { VeryBasic } from './models/very-basic.model';
import { VeryBasicState } from './state/very-basic.state';


export interface BasicStateModel {
    basic: Basic[];
}
@State<BasicStateModel>({
    name: 'basic',
    defaults: {basic: [Basic.withValues(1, 'Tomi'), Basic.withValues(654, 'Valaki')]},
    children: [VeryBasicState]
})
export class BasicState {

    @Selector()
    static ids(state: any) {
        return state.basic.map((basic: Basic) => basic.id);
    }
    
    @Selector()
    static names(state: any) {
        return state.basic.map((basic: Basic) => basic.name);
    }

    @Action(AddBasic)
    add(context: StateContext<BasicStateModel>, action: AddBasic) {
        const state = context.getState();
        context.patchState({
            basic: [...state.basic, action.payload]
        });
    }

    @Action(RemoveBasic)
    remove(context: StateContext<BasicStateModel>, action: RemoveBasic) {
        const state = context.getState();
        context.patchState({
            basic:  state.basic.filter((basic: Basic) => basic.id !== action.id)
        });
    }
}